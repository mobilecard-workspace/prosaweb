<%-- 
    Document   : index
    Created on : 12/07/2012, 10:05:40 AM
    Author     : ADDCEL13
--%>
<%@page import="com.addcel.prosa.ProsaDataTransaction"%>
<%@page import="com.addcel.prosa.ProsaDataDev"%>
<%@page import="com.addcel.prosa.ProsaData"%>
<%@page import="com.addcel.prosa.ProsaTransaction"%>
<%@page import="com.addcel.prosa.ProsaResponse"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

        String mode = request.getParameter("mode");
        String action = request.getParameter("action");
        String reversalIdn = request.getParameter("reversal");

        String modoEntrada = request.getParameter("modo");
        String capacidadPin = request.getParameter("pin");
        String conditionCode = request.getParameter("code");
        String capacidadTerminal = request.getParameter("capacidad");
        String ubicacionTerminal = request.getParameter("ubicacion");
        String importe = request.getParameter("importe");
        String tarjeta = request.getParameter("tarjeta");
        String vigencia = request.getParameter("vigencia");
        String cvv = request.getParameter("cvv");
        String afiliacion = request.getParameter("afiliacion");
        String numeroCaja = request.getParameter("caja");
        String moneda = request.getParameter("moneda");
        String claveVenta = request.getParameter("clave");
        String operador = request.getParameter("operador");
        String nombre = request.getParameter("nombre");
        String terminal = request.getParameter("terminal");
        String fiid = request.getParameter("fiid");
        String indMedioAcceso = request.getParameter("medio");

        ProsaDataTransaction prosaData = new ProsaDataTransaction();
        
        if(mode==null || mode.equals(""))
        {
            prosaData.setModoEntrada(modoEntrada);
            prosaData.setCapacidadPin(capacidadPin);
            prosaData.setConditionCode(conditionCode);
            prosaData.setCapacidadTerminal(capacidadTerminal);
            prosaData.setUbicacionTerminal(ubicacionTerminal);
            prosaData.setImporte(importe);
            prosaData.setTrack2(tarjeta+"="+vigencia);
            prosaData.setCvv2(cvv);
            prosaData.setAfiliacion(afiliacion);
            prosaData.setNumeroCaja(numeroCaja);
            prosaData.setMoneda(moneda);
            prosaData.setModoEntrada(modoEntrada);
            prosaData.setClaveVenta(claveVenta);
            prosaData.setOperador(operador);
            prosaData.setNombre(nombre);
            prosaData.setTerminal(terminal);
            prosaData.setFiid(fiid);
            prosaData.setIndMedioAcceso(indMedioAcceso);

        }
        else if(mode.equals("N"))
        {
            action ="V";
            reversalIdn = "";
            prosaData.setModoEntrada("01");
            prosaData.setCapacidadPin("1");
            prosaData.setConditionCode("00");
            prosaData.setCapacidadTerminal("8");
            prosaData.setUbicacionTerminal("0");
            prosaData.setImporte("1.0");
            //prosaData.setTrack2("4555402090709378=0616");
            prosaData.setTrack2("5221740000001126=1213");
            prosaData.setCvv2("234");
            //prosaData.setAfiliacion("7349736");
            prosaData.setAfiliacion("9165713");
            prosaData.setNumeroCaja("99");
            prosaData.setMoneda("484");
            prosaData.setClaveVenta("537616");
            prosaData.setOperador("96597");
            prosaData.setNombre("PRUEBAS QA");
            prosaData.setTerminal("123");
            prosaData.setFiid("B012");
            prosaData.setIndMedioAcceso("03");
        }
        
        
        ProsaResponse res = new ProsaResponse();
        if(action.equals("V"))
            //res = (new ProsaTransaction()).authorization(prosaData);
            res = (new ProsaTransaction()).authorization("4555402090709378", "0616", "256", "22.0", "Aurelio Catro Gonzalez", "7175645");
        else if(action.equals("R"))
            res = (new ProsaTransaction()).reversal(reversalIdn);
        else if(action.equals("D"))
            res = (new ProsaTransaction()).devolution(reversalIdn);
            
        
        
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    -------------------------<br/>
        <%=res.isIsAuthorized()%><br/>
        <%=res.isIsRejected()%><br/>
        <%=res.isIsProsaError()%><br/>
    <br/>
    -------------------------<br/>
        <%
            //res.transaccion 
         %>
    -------------------------<br/>
        <%=res.error%>
    -------------------------<br/>
        <%=res.msg %>
    <br/>
    </body>
</html>
