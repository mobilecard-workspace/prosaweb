/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.servlet;

import com.addcel.common.MCUser;
import com.addcel.prosa.ProsaTransaction;
import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Salvador
 */
public class ProsaAuth extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("Prosa:init...");
        String json="";
        
        try
        {
            String card = request.getParameter("card");
            String vigencia = request.getParameter("vigencia");
            String nombre = request.getParameter("nombre");
            String user = request.getParameter("user");
            String cvv2 = request.getParameter("cvv2");
            String monto = request.getParameter("monto");
            String afiliacion = request.getParameter("afiliacion");
            String moneda = request.getParameter("moneda");
            
            if(moneda==null || moneda.equals(""))
            {
                moneda="484";
            }
            
            //mientras
            //moneda="484";

            System.out.println("*********************ECOMMERCE***********************\n");
            System.out.println("Sengin Auth...\n");
            System.out.println("card="+card);
            System.out.println("vigencia="+vigencia);
            System.out.println("cvv2="+cvv2);
            System.out.println("monto="+monto);
            System.out.println("nombre="+nombre);
            System.out.println("afiliacion="+afiliacion);

            Gson gson = new Gson();
            json = gson.toJson((new ProsaTransaction()).authorization(card, vigencia, cvv2, monto, nombre,afiliacion,moneda));
        }
        catch(Exception ex)
        {
            json="{\"error\":\""+ex.getMessage()+"\"}";
        }
        response.getWriter().println(json);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
