/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
//import org.jdom2.xpath.XPathFactory;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Salvador
 */
public class ProsaXMLResponse {
    
    private String xmlMessage;
    
    private String prosaResponse;
    private boolean aprobada;
    private boolean rechazada;
    private boolean errorProsa;
    
//    public String bancoAdquirente;
//    public String nombreComercio;
//    public String direccionComercio;
//    public String afiliacion;
//    public String importe;
//    public String moneda;
//    public String fechaHora;
//    public String claveOperacion;
//    public String numeroCaja;
//    public String producto;
//    public String bancoEmisor;
//    public String marca;
//    public String autorizacion;
//    public String poblacion;
    
    
    public ProsaXMLResponse(String xmlMessage)
    {
        this.xmlMessage = xmlMessage;
    }
    
    public ProsaResponse getResponse(ProsaResponse response)
    {
        
        if(isAprobada())
        {
            response.setIsAuthorized(true);
            response.setIsRejected(false);
            response.setIsProsaError(false);
            response.setBancoAdquirente(process("//bancoAdquirente"));
            response.setNombreComercio(process("//nombreComercio"));
            response.setDireccionComercio(process("//direccionComercio"));
            response.setAfiliacion(process("//afiliacion"));
            response.setImporte(process("//importe"));
            response.setMoneda(process("//moneda"));
            response.setFechaHora(process("//fechaHora"));
            response.setClaveOperacion(process("//claveOperacion"));
            response.setNumeroCaja(process("//numeroCaja"));
            response.setProducto(process("//producto"));
            response.setBancoEmisor(process("//bancoEmisor"));
            response.setMarca(process("//marca"));
            response.setAutorizacion(process("//autorizacion"));
            response.setPoblacion(process("//poblacion"));
            response.setProducto(process("//producto"));
        }
        else if(isRechazada())
        {
            response.setIsAuthorized(false);
            response.setIsRejected(true);
            response.setIsProsaError(false);
            response.setBancoAdquirente(process("//bancoAdquirente"));
            response.setNombreComercio(process("//nombreComercio"));
            response.setDireccionComercio(process("//direccionComercio"));
            response.setAfiliacion(process("//afiliacion"));
            response.setImporte(process("//importe"));
            response.setMoneda(process("//moneda"));
            response.setFechaHora(process("//fechaHora"));
            response.setClaveOperacion(process("//claveOperacion"));
            response.setClaveRechazo(process("//claveRechazo"));
            response.setDescripcionRechazo(process("//descripcionRechazo"));
            
        }
        else
        {
            response.setIsAuthorized(false);
            response.setIsRejected(false);
            response.setIsProsaError(true);
            response.msg = process("//descripcion");
            response.error = process("//error");
        }
        return response;
    }
    
    public boolean isAprobada()
    {
        String sAprobada = process("//aprobada");
        if(sAprobada==null)
            return false;
        else
            return true;
    }
    
    public boolean isRechazada()
    {
        String sRechazada = process("//rechazada");
        if(sRechazada==null)
            return false;
        else
            return true;
    }
    
    public boolean isErrorProsa()
    {
        String sError = process("//errorProsa");
        if(sError==null)
            return false;
        else
            return true;
    }
    
    
    
    public String getClaveOperacion()
    {
        return process("//aprobada/claveOperacion");
    }

    public String getAutorizacion()
    {
        return process("//aprobada/autorizacion");
    }
    
    
    private String process(String query)
    {
        String ret = null;
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        
        try
        {
            xmlMessage = xmlMessage.replaceAll("xmlns=\"http://www.prosa.com.mx/xsd/\"", "xmlns:journal=\"http://www.w3.org/2001/XMLSchema-Instance\"");
            InputStream is = new ByteArrayInputStream(xmlMessage.getBytes("UTF-8"));
            InputSource inputSource = new InputSource(is);
            //NodeSet node = (NodeSet)(xpath.evaluate("/Transaccion",inputSource,XPathConstants.NODESET));
            NodeList nodes = (NodeList)(xpath.evaluate(query,inputSource,XPathConstants.NODESET));                        
            
            if(nodes==null)
                System.out.println("null");
            else if(nodes.getLength() > 0)
            {
                for(int i=0; i<nodes.getLength(); i++)                    
                {
                    Node node = nodes.item(i);
                    if(!node.getNodeName().equals("#text"))
                    {
                        if (node.getTextContent()!=null)
                        {
                            ret = node.getTextContent();
                        }
                    }
                }
                
            }

        }
        catch(Exception e)
        {
            System.err.println(e.getMessage());
        }
        return ret;
    }
}
