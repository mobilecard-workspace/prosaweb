/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

/**
 *
 * @author ADDCEL13
 */
public class ProsaData extends ProsaDataTransaction{
    
    private String idn = "";
    private String claveOperacion;
    private String autorizacion;
    
    public static ProsaData convertToProsaData(ProsaDataTransaction data)
    {
        ProsaData prosaData = new ProsaData();
        prosaData.setModoEntrada(data.getModoEntrada());
        prosaData.setCapacidadPin(data.getCapacidadPin());
        prosaData.setConditionCode(data.getConditionCode());
        prosaData.setCapacidadTerminal(data.getCapacidadTerminal());
        prosaData.setUbicacionTerminal(data.getUbicacionTerminal());
        prosaData.setImporte(data.getImporte());
        prosaData.setTrack2(data.getTrack2());
        prosaData.setFechaHora(data.getFechaHora());
        prosaData.setFechaAplicacion(data.getFechaAplicacion());
        prosaData.setAfiliacion(data.getAfiliacion());
        prosaData.setNumeroCaja(data.getNumeroCaja());
        prosaData.setMoneda(data.getMoneda());
        prosaData.setClaveVenta(data.getClaveVenta());
        prosaData.setOperador(data.getOperador());
        prosaData.setNombre(data.getNombre());
        prosaData.setTerminal(data.getTerminal());
        prosaData.setFiid(data.getFiid());
        prosaData.setIndMedioAcceso(data.getIndMedioAcceso());        
        
        return prosaData;
    }
    
    public static ProsaDataDev convertToProsaDataDev(ProsaData data,String claveOperacion, String autorizacion)
    {
        ProsaDataTransaction dataTrans = new ProsaDataTransaction(data);
        ProsaDataDev dataDev = new ProsaDataDev(dataTrans);
        dataDev.setClaveOperacion(claveOperacion);
        dataDev.setAutorizacion(autorizacion);
        
        return dataDev;
    }


    /**
     * @return the idn
     */
    public String getIdn() {
        return idn;
    }

    /**
     * @param idn the idn to set
     */
    public void setIdn(String idn) {
        this.idn = idn;
    }

    /**
     * @return the claveOperacion
     */
    public String getClaveOperacion() {
        return claveOperacion;
    }

    /**
     * @param claveOperacion the claveOperacion to set
     */
    public void setClaveOperacion(String claveOperacion) {
        this.claveOperacion = claveOperacion;
    }

    /**
     * @return the autorizacion
     */
    public String getAutorizacion() {
        return autorizacion;
    }

    /**
     * @param autorizacion the autorizacion to set
     */
    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }
    
    
}
