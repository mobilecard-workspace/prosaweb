/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

import com.addcel.tools.util.DateUtil;
import com.prosa.mx.seguridad.RsaProsa;

/**
 *
 * @author ADDCEL13
 */
public class ProsaDataTransaction {

    private String modoEntrada;
    private String capacidadPin;
    private String conditionCode;
    private String capacidadTerminal;
    private String ubicacionTerminal;
    private String importe;
    private String track2;
    private String cvv2;
    private String fechaHora;
    private String fechaAplicacion;
    private String afiliacion;
    private String numeroCaja;
    private String moneda;
    private String claveVenta;
    private String operador;
    private String nombre;
    private String terminal;
    private String fiid;
//    private String emvRequest;
    private String indMedioAcceso;
//    private String claveOperacion;
//    private String autorizacion;
    
//    private String key;
    public ProsaDataTransaction(ProsaData data)
    {
        this.modoEntrada = data.getModoEntrada();
        this.capacidadPin = data.getCapacidadPin();
        this.conditionCode = data.getConditionCode();
        this.capacidadTerminal = data.getCapacidadTerminal();
        this.ubicacionTerminal = data.getUbicacionTerminal();
        this.importe = data.getImporte();
        this.track2 = data.getTrack2();
        this.cvv2 = data.getCvv2();
        this.fechaHora = data.getFechaHora();
        this.fechaAplicacion = data.getFechaAplicacion();
        this.afiliacion = data.getAfiliacion();
        this.numeroCaja = data.getNumeroCaja();
        this.moneda = data.getMoneda();
        this.claveVenta = data.getClaveVenta();
        this.operador = data.getOperador();
        this.nombre = data.getNombre();
        this.terminal = data.getTerminal();
        this.fiid = data.getFiid();
        this.indMedioAcceso = data.getIndMedioAcceso();        
    }
    public ProsaDataTransaction() {
        this.modoEntrada = "";
        this.capacidadPin = "";
        this.conditionCode = "";
        this.capacidadTerminal = "";
        this.ubicacionTerminal = "";
        this.importe = "";
        this.track2 = "";
        this.cvv2 = "";
        this.fechaHora = DateUtil.getNowDate();
        this.fechaAplicacion = DateUtil.getNowDate();
        this.afiliacion = "";
        this.numeroCaja = "";
        this.moneda = "";
        this.claveVenta = "";
        this.operador = "";
        this.nombre = "";
        this.terminal = "";
        this.fiid = "";
        this.indMedioAcceso = "";
    }

    @Override
    public String toString() {
        return 
                "ProsaDataObj:\n"+
                "\tmodoEntrada="+modoEntrada+"\n"+
                "\tcapacidadPin="+capacidadPin+"\n"+
                "\tconditionCode="+conditionCode+"\n"+
                "\tcapacidadTerminal="+capacidadTerminal+"\n"+
                "\tubicacionTerminal="+ubicacionTerminal+"\n"+
                "\timporte="+importe+"\n"+
                "\ttrack2="+track2+"\n"+
                "\tfechaHora="+fechaHora+"\n"+
                "\tfechaAplicacion="+fechaAplicacion+"\n"+
                "\tafiliacion="+afiliacion+"\n"+
                "\tnumeroCaja="+numeroCaja+"\n"+
                "\tmoneda="+moneda+"\n"+
                "\tclaveVenta="+claveVenta+"\n"+
                "\toperador="+operador+"\n"+
                "\tnombre="+nombre+"\n"+
                "\tterminal="+terminal+"\n"+
                "\tfiid="+fiid+"\n"+
                "\tindMedioAcceso="+indMedioAcceso+"\n";
    }

    
  
    /**
     * @return the modoEntrada
     */
    public String getModoEntrada() {
        return modoEntrada;
    }

    /**
     * @param modoEntrada the modoEntrada to set
     */
    public void setModoEntrada(String modoEntrada) {
        this.modoEntrada = modoEntrada;
    }

    /**
     * @return the capacidadPin
     */
    public String getCapacidadPin() {
        return capacidadPin;
    }

    /**
     * @param capacidadPin the capacidadPin to set
     */
    public void setCapacidadPin(String capacidadPin) {
        this.capacidadPin = capacidadPin;
    }

    /**
     * @return the conditionCode
     */
    public String getConditionCode() {
        return conditionCode;
    }

    /**
     * @param conditionCode the conditionCode to set
     */
    public void setConditionCode(String conditionCode) {
        this.conditionCode = conditionCode;
    }

    /**
     * @return the capacidadTerminal
     */
    public String getCapacidadTerminal() {
        return capacidadTerminal;
    }

    /**
     * @param capacidadTerminal the capacidadTerminal to set
     */
    public void setCapacidadTerminal(String capacidadTerminal) {
        this.capacidadTerminal = capacidadTerminal;
    }

    /**
     * @return the ubicacionTerminal
     */
    public String getUbicacionTerminal() {
        return ubicacionTerminal;
    }

    /**
     * @param ubicacionTerminal the ubicacionTerminal to set
     */
    public void setUbicacionTerminal(String ubicacionTerminal) {
        this.ubicacionTerminal = ubicacionTerminal;
    }

    /**
     * @return the importe
     */
    public String getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(String importe) {
        this.importe = importe;
    }

    /**
     * @return the track2
     */
    public String getTrack2() {
        return track2;
    }

    /**
     * @param track2 the track2 to set
     */
    public void setTrack2(String track2) {
        RsaProsa rsaProsa =new RsaProsa( );
        try
        {
            this.track2 = rsaProsa.byteToHex(rsaProsa.encrypt(track2));
        }
        catch(Exception e)
        {
            this.track2 = "--";
        }
    }
    
    public void _setTrack2(String track2) {
        this.track2 = track2;
    }

    
    /**
     * @return the cvv2
     */
    public String getCvv2() {
        return cvv2;
    }

    /**
     * @param cvv2 the cvv2 to set
     */
    public void setCvv2(String cvv2) {
        RsaProsa rsaProsa =new RsaProsa( );
        try
        {
            this.cvv2 = rsaProsa.byteToHex(rsaProsa.encrypt(cvv2));
        }
        catch(Exception e)
        {
            this.cvv2 = "--";
        }        
    }

    public void _setCvv2(String cvv2) {
            this.cvv2 = cvv2;
    }
    
    
    /**
     * @return the fechaHora
     */
    public String getFechaHora() {
        return fechaHora;
    }

    /**
     * @param fechaHora the fechaHora to set
     */
    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    /**
     * @return the fechaAplicacion
     */
    public String getFechaAplicacion() {
        return fechaAplicacion;
    }

    /**
     * @param fechaAplicacion the fechaAplicacion to set
     */
    public void setFechaAplicacion(String fechaAplicacion) {
        this.fechaAplicacion = fechaAplicacion;
    }

    /**
     * @return the afiliacion
     */
    public String getAfiliacion() {
        return afiliacion;
    }

    /**
     * @param afiliacion the afiliacion to set
     */
    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    /**
     * @return the numeroCaja
     */
    public String getNumeroCaja() {
        return numeroCaja;
    }

    /**
     * @param numeroCaja the numeroCaja to set
     */
    public void setNumeroCaja(String numeroCaja) {
        this.numeroCaja = numeroCaja;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the claveVenta
     */
    public String getClaveVenta() {
        return claveVenta;
    }

    /**
     * @param claveVenta the claveVenta to set
     */
    public void setClaveVenta(String claveVenta) {
        this.claveVenta = claveVenta;
    }

    /**
     * @return the operador
     */
    public String getOperador() {
        return operador;
    }

    /**
     * @param operador the operador to set
     */
    public void setOperador(String operador) {
        this.operador = operador;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the terminal
     */
    public String getTerminal() {
        return terminal;
    }

    /**
     * @param terminal the terminal to set
     */
    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    /**
     * @return the fiid
     */
    public String getFiid() {
        return fiid;
    }

    /**
     * @param fiid the fiid to set
     */
    public void setFiid(String fiid) {
        this.fiid = fiid;
    }

//    /**
//     * @return the emvRequest
//     */
//    public String getEmvRequest() {
//        return emvRequest;
//    }
//
//    /**
//     * @param emvRequest the emvRequest to set
//     */
//    public void setEmvRequest(String emvRequest) {
//        this.emvRequest = emvRequest;
//    }

    /**
     * @return the indMedioAcceso
     */
    public String getIndMedioAcceso() {
        return indMedioAcceso;
    }

    /**
     * @param indMedioAcceso the indMedioAcceso to set
     */
    public void setIndMedioAcceso(String indMedioAcceso) {
        this.indMedioAcceso = indMedioAcceso;
    }

//    /**
//     * @return the key
//     */
//    public String getKey() {
//        return key;
//    }
//
//    /**
//     * @param key the key to set
//     */
//    public void setKey(String key) {
//        this.key = key;
//    }
//    

//    /**
//     * @return the claveOperacion
//     */
//    public String getClaveOperacion() {
//        return claveOperacion;
//    }
//
//    /**
//     * @param claveOperacion the claveOperacion to set
//     */
//    public void setClaveOperacion(String claveOperacion) {
//        this.claveOperacion = claveOperacion;
//    }
//
//    /**
//     * @return the autorizacion
//     */
//    public String getAutorizacion() {
//        return autorizacion;
//    }
//
//    /**
//     * @param autorizacion the autorizacion to set
//     */
//    public void setAutorizacion(String autorizacion) {
//        this.autorizacion = autorizacion;
//    }



    
}
