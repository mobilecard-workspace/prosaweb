/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

import com.addcel.tools.util.Constantes;
import com.addcel.tools.util.DateUtil;
import com.addcel.tools.util.TransactionControl;
import com.prosa.mx.seguridad.DigestProsa;
import com.prosa.mx.seguridad.RsaProsa;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.pojoxml.core.PojoXml;
import org.pojoxml.core.PojoXmlFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author ADDCEL13
 */
public class ProsaTransaction {

    public ProsaResponse reversal(String transactionIdn) {
        ProsaData dataTrans = TransactionControl.getData(transactionIdn);
        return reversal(dataTrans);
    }

    public ProsaResponse reversal(ProsaData data) {
        ProsaData dataTrans = data;//TransactionControl.getData(transactionIdn);
        ProsaResponse prosaResponse;
        String seguridad = "";
        String transaccion = "";
        String ret = "";

        ProsaDataTransaction prosaData = new ProsaDataTransaction(dataTrans);
        System.out.println(prosaData);

        try {
            prosaResponse = new ProsaResponse();
            //comentado para QA
            seguridad = createSecurityReversal(prosaData);
            transaccion = createReversal(prosaData);
            prosaResponse.setSeguridad(seguridad);
            prosaResponse.setTransaccion(transaccion);

            System.out.println("REVERSAL:");
            System.out.println(transaccion);

            ProsaWS ws = new ProsaWS(transaccion, seguridad);
            //Solo para QA, VVLIRA 1 agosto 2013
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                ret = ws.getReversa();
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                ret = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                        + " <Transaccion xmlns=\"http://www.prosa.com.mx/xsd/\"><reversoRequest>\n"
                        + "   <modoEntrada>01</modoEntrada>\n"
                        + "   <capacidadPin>1</capacidadPin>\n"
                        + "   <conditionCode>00</conditionCode>\n"
                        + "   <capacidadTerminal>8</capacidadTerminal>\n"
                        + "   <ubicacionTerminal>0</ubicacionTerminal>\n"
                        + "   <importe>"+prosaData.getImporte()+"</importe>\n"
                        + "   <track2>"+prosaData.getTrack2()+"</track2>\n"
                        + "   <cvv2>"+prosaData.getCvv2()+"</cvv2>\n"
                        + "   <fechaHora>"+prosaData.getFechaHora()+"</fechaHora>\n"
                        + "   <fechaAplicacion>"+prosaData.getFechaAplicacion()+"</fechaAplicacion>\n"
                        + "   <afiliacion>4533485</afiliacion>\n"
                        + "   <numeroCaja>99</numeroCaja>\n"
                        + "   <moneda>484</moneda>\n"
                        + "   <claveVenta>537616</claveVenta>\n"
                        + "   <operador>96597</operador>\n"
                        + "   <nombre>ADDCEL PROSA</nombre>\n"
                        + "   <terminal>123</terminal>\n"
                        + "   <fiid>"+prosaData.getFiid()+"</fiid>\n"
                        + "   <indMedioAcceso>03</indMedioAcceso>\n"
                        + " </reversoRequest></Transaccion>";
            }
            prosaResponse.error = "0";
            prosaResponse.msg = ret;
            System.out.println("PROSA REVERSA:\n" + ret);

        } catch (Exception e) {
            System.err.println(e);
            prosaResponse = new ProsaResponse("666", e.getMessage());
        }
        return prosaResponse;
    }

    public ProsaResponse authorization(String card, String vigencia, String cvv2, String monto, String nombre, String afiliacion, String moneda) {
        //Corrigiendo el error horripilante del Pott
        vigencia = vigencia.replace("/", "");
        vigencia = vigencia.replace(" ", "");

        //Swaping expire date
        String part1 = vigencia.substring(0, 2);
        String part2 = vigencia.substring(2, 4);

        String vigenciaSwap = part2 + part1;
        System.out.println("Vigencia Swap=" + vigenciaSwap);

        ProsaDataTransaction prosaData = new ProsaDataTransaction();
        prosaData.setModoEntrada("01");
        prosaData.setCapacidadPin("1");
        prosaData.setConditionCode("00");
        prosaData.setCapacidadTerminal("8");
        prosaData.setUbicacionTerminal("0");
        prosaData.setImporte(monto);
        //prosaData.setTrack2("4555402090709378=0616");
        prosaData.setTrack2(card + "=" + vigenciaSwap);
        prosaData.setCvv2(cvv2);
        //prosaData.setAfiliacion("7349736");
        prosaData.setAfiliacion(afiliacion);
        prosaData.setNumeroCaja("99");
        prosaData.setMoneda(moneda);
        prosaData.setClaveVenta("537616");
        prosaData.setOperador("96597");
        //prosaData.setNombre(nombre);
        prosaData.setNombre("ADDCEL PROSA");
        prosaData.setTerminal("123");
        prosaData.setFiid("B012");
        prosaData.setIndMedioAcceso("03");



        //ProsaResponse response = authorization(prosaData);

        return authorization(prosaData);


    }

    public ProsaResponse authorization(String card, String vigencia, String cvv2, String monto, String nombre, String afiliacion) {
        return authorization(card, vigencia, cvv2, monto, nombre, afiliacion, "484");
    }

    public ProsaResponse authorization(ProsaDataTransaction prosaData) {
        String seguridad = "";
        String transaccion = "";
        String ret = "";

        ProsaResponse prosaResponse = null;
        ProsaXMLResponse response = null;
        try {
            prosaResponse = new ProsaResponse();
            //comentado para QA
            seguridad = createSecurityTransaction(prosaData);
            transaccion = createTransaction(prosaData);
            prosaResponse.setSeguridad(seguridad);
            prosaResponse.setTransaccion(transaccion);

            ProsaWS ws = new ProsaWS(transaccion, seguridad);
            System.out.println("PROSA AUTH SEG:\n" + seguridad + "\n\n");
            System.out.println("PROSA AUTH DATA:\n" + transaccion + "\n\n");
            //Solo para QA, VVLIRA 1 agosto 2013
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                ret = ws.getVenta();
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                if (true||((int) (Math.random() * 100)) % 9 == 1) {
                    ret = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
                            + " <Transaccion xmlns=\"http://www.prosa.com.mx/xsd/\">\n"
                            + "     <ventaResponse>\n"
                            + "         <aprobada>\n"
                            + "             <bancoAdquirente>HSBC</bancoAdquirente>\n"
                            + "             <nombreComercio>ADDCEL</nombreComercio>\n"
                            + "             <direccionComercio>AV PASEO DE LA REFOR NO 2654 P</direccionComercio>\n"
                            + "             <afiliacion>7175645</afiliacion>\n"
                            + "             <track2>" + prosaData.getTrack2() + "</track2>\n"
                            + "             <importe>" + prosaData.getImporte() + "</importe>\n"
                            + "             <moneda>MXN</moneda>\n"
                            + "             <fechaHora>" + prosaData.getFechaAplicacion() + "</fechaHora>\n"
                            + "             <claveOperacion>" + ((int) Math.random() * 10000000) + "</claveOperacion>\n"
                            + "             <numeroCaja>99</numeroCaja>\n"
                            + "             <producto>" + ((((int) (Math.random() * 100)) % 2 == 1) ? "CREDITO" : "DEBITO") + "</producto>\n"
                            + "             <bancoEmisor>OTROS</bancoEmisor>\n"
                            + "             <marca>" + ((((int) (Math.random() * 100)) % 2 == 1) ? "VISA" : "MASTER CARD") + "</marca>\n"
                            + "             <autorizacion>" + (Constantes.NUMERO_AUTORIZACION++) + "</autorizacion>\n"
                            + "             <poblacion>MEXICO</poblacion>\n"
                            + "         </aprobada>\n"
                            + "     </ventaResponse>\n"
                            + " </Transaccion>";
                } else {
                    ret = " <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
                            + " <Transaccion xmlns=\"http://www.prosa.com.mx/xsd/\">\n"
                            + "     <ventaResponse>\n"
                            + "         <rechazada>\n"
                            + "             <bancoAdquirente>HSBC</bancoAdquirente>\n"
                            + "             <nombreComercio>IAVE ENLAC TOR SURCR75</nombreComercio>\n"
                            + "             <direccionComercio>VIA DE LA BARRANCA NO 6 P 6</direccionComercio>\n"
                            + "             <afiliacion>4533485</afiliacion>\n"
                            + "             <track2>" + prosaData.getTrack2() + "</track2>\n"
                            + "             <importe>" + prosaData.getImporte() + "</importe>\n"
                            + "             <moneda>484</moneda>\n"
                            + "             <fechaHora>" + prosaData.getFechaAplicacion() + "</fechaHora>\n"
                            + "             <claveOperacion>" + ((int) Math.random() * 10000000) + "</claveOperacion>\n"
                            + "             <claveRechazo>56</claveRechazo>\n"
                            + "             <descripcionRechazo>LLAME AL BCO. EMISOR</descripcionRechazo>\n"
                            + "         </rechazada>\n"
                            + "     </ventaResponse>\n"
                            + " </Transaccion>";
                }
            }

            prosaResponse.error = "0";
            prosaResponse.msg = ret;
            System.out.println("PROSA VENTA:\n" + ret);
            int transaccionIdn = TransactionControl.createTransaction(prosaData);
            response = new ProsaXMLResponse(ret);
            prosaResponse = response.getResponse(prosaResponse);
            prosaResponse.setTransactionId(transaccionIdn + "");

//            //Verificando 
//            if(response.isAprobada())
//            {
//                String claveOperacion = response.getClaveOperacion();
//                String autorizacion = response.getAutorizacion();
//            }
            TransactionControl.updateTransaction(transaccionIdn, prosaResponse);


        } catch (Exception e) {
            System.err.println(e);
            System.out.println("ERROR CATCH:" + e.getMessage());
            prosaResponse = new ProsaResponse("666", e.getMessage());
            reversal(ProsaData.convertToProsaData(prosaData));
        }

        return prosaResponse;
    }

    public ProsaResponse devolution(String transactionIdn) {
        String seguridad = "";
        String transaccion = "";
        String ret = "";

        System.out.println("DEVOLUCION TransactionID: " + transactionIdn);

        ProsaResponse prosaResponse = null;
        ProsaXMLResponse response = null;
        try {
            ProsaData data;
            data = TransactionControl.getData(transactionIdn);
            ProsaDataDev prosaData = ProsaData.convertToProsaDataDev(data, data.getClaveOperacion(), data.getAutorizacion());
            prosaResponse = new ProsaResponse();
            seguridad = createSecurityDevolution(prosaData);
            transaccion = createDevolution(prosaData);
            prosaResponse.setSeguridad(seguridad);
            prosaResponse.setTransaccion(transaccion);

            System.out.println("PROSA DATA DEVOLUCION: " + transaccion);
            System.out.println(seguridad);
            System.out.println(transaccion);

            ProsaWS ws = new ProsaWS(transaccion, seguridad);
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                ret = ws.getDevolution();
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
            }


            prosaResponse.error = "0";
            prosaResponse.msg = ret;

            response = new ProsaXMLResponse(ret);
            prosaResponse = response.getResponse(prosaResponse);

            System.out.println("PROSA DEVOLUCION:\n" + ret);
            TransactionControl.createDevolution(prosaData, prosaResponse, transactionIdn);
            prosaResponse.setTransactionId(transactionIdn);
//            System.out.println(ret);

        } catch (Exception e) {
            System.err.println(e);
            prosaResponse = new ProsaResponse("666", e.getMessage());
        }

        return prosaResponse;
    }

    private String createTransaction(ProsaDataTransaction prosaData) {
        PojoXml pojoxml = PojoXmlFactory.createPojoXml();
        String s = pojoxml.getXml(prosaData);
        s = s.replaceAll("<ProsaDataTransaction>", "<Transaccion xmlns=\"http://www.prosa.com.mx/xsd/\"><ventaRequest>");
        s = s.replaceAll("</ProsaDataTransaction>", "</ventaRequest></Transaccion>");
        //System.out.println("\n******Transaction:"+s+"\n");
        return s;

    }

    private String createReversal(ProsaDataTransaction prosaData) {
        PojoXml pojoxml = PojoXmlFactory.createPojoXml();
        String s = pojoxml.getXml(prosaData);
        s = s.replaceAll("<ProsaDataTransaction>", "<Transaccion xmlns=\"http://www.prosa.com.mx/xsd/\"><reversoRequest>");
        s = s.replaceAll("</ProsaDataTransaction>", "</reversoRequest></Transaccion>");
        //System.out.println("\n******Transaction:"+s+"\n");
        return s;
    }

    private String createDevolution(ProsaDataDev prosaData) {
        PojoXml pojoxml = PojoXmlFactory.createPojoXml();
        String s = pojoxml.getXml(prosaData);
        s = s.replaceAll("<ProsaDataDev>", "<Transaccion xmlns=\"http://www.prosa.com.mx/xsd/\"><devolucionRequest>");
        s = s.replaceAll("</ProsaDataDev>", "</devolucionRequest></Transaccion>");
        //System.out.println("\n******Transaction:"+s+"\n");
        return s;
    }

    private String createSecurityTransaction(ProsaDataTransaction prosaData) {
        String res = "";
        String keyData = "";

        RsaProsa rsaProsa = new RsaProsa();
        DigestProsa datos1 = new DigestProsa();
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Attr attr = null;

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("SeguridadProsa");
            attr = doc.createAttribute("xmlns");
            attr.setValue("http://www.prosa.com.mx/xsd/");
            rootElement.setAttributeNode(attr);
            doc.appendChild(rootElement);

            Element xmlKey = doc.createElement("key");
            xmlKey.appendChild(doc.createTextNode(CrearString.crea()));
            rootElement.appendChild(xmlKey);

            Element xmlKeyData = doc.createElement("keyData");
            String s = createTransaction(prosaData);

            try {
                keyData = datos1.getDigest(s.getBytes());
                keyData = rsaProsa.byteToHex(rsaProsa.encrypt(keyData));
            } catch (Exception e) {
                keyData = "--";
            }



            xmlKeyData.appendChild(doc.createTextNode(keyData));
            rootElement.appendChild(xmlKeyData);


            Element xmlFechaHora = doc.createElement("fechaHora");
            xmlFechaHora.appendChild(doc.createTextNode((DateUtil.getNowDate())));
            rootElement.appendChild(xmlFechaHora);

            Element xmlUser = doc.createElement("user");
            xmlUser.appendChild(doc.createTextNode("USRADCCE02"));
            rootElement.appendChild(xmlUser);


            res = getStringFromDocument(doc);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        return res;

    }

    private String createSecurityReversal(ProsaDataTransaction prosaData) {
        String res = "";
        String keyData = "";

        RsaProsa rsaProsa = new RsaProsa();
        DigestProsa datos1 = new DigestProsa();
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Attr attr = null;

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("SeguridadProsa");
            attr = doc.createAttribute("xmlns");
            attr.setValue("http://www.prosa.com.mx/xsd/");
            rootElement.setAttributeNode(attr);
            doc.appendChild(rootElement);

            Element xmlKey = doc.createElement("key");
            xmlKey.appendChild(doc.createTextNode(CrearString.crea()));
            rootElement.appendChild(xmlKey);

            Element xmlKeyData = doc.createElement("keyData");
            String s = createReversal(prosaData);

            try {
                keyData = datos1.getDigest(s.getBytes());
                keyData = rsaProsa.byteToHex(rsaProsa.encrypt(keyData));
            } catch (Exception e) {
                keyData = "--";
            }



            xmlKeyData.appendChild(doc.createTextNode(keyData));
            rootElement.appendChild(xmlKeyData);


            Element xmlFechaHora = doc.createElement("fechaHora");
            xmlFechaHora.appendChild(doc.createTextNode((DateUtil.getNowDate())));
            rootElement.appendChild(xmlFechaHora);

            Element xmlUser = doc.createElement("user");
            xmlUser.appendChild(doc.createTextNode("USRADCCE02"));
            rootElement.appendChild(xmlUser);


            res = getStringFromDocument(doc);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        return res;

    }

    private String createSecurityDevolution(ProsaDataDev prosaData) {
        String res = "";
        String keyData = "";

        RsaProsa rsaProsa = new RsaProsa();
        DigestProsa datos1 = new DigestProsa();
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Attr attr = null;

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("SeguridadProsa");
            attr = doc.createAttribute("xmlns");
            attr.setValue("http://www.prosa.com.mx/xsd/");
            rootElement.setAttributeNode(attr);
            doc.appendChild(rootElement);

            Element xmlKey = doc.createElement("key");
            xmlKey.appendChild(doc.createTextNode(CrearString.crea()));
            rootElement.appendChild(xmlKey);

            Element xmlKeyData = doc.createElement("keyData");
            String s = createDevolution(prosaData);

            try {
                keyData = datos1.getDigest(s.getBytes());
                keyData = rsaProsa.byteToHex(rsaProsa.encrypt(keyData));
            } catch (Exception e) {
                keyData = "--";
            }



            xmlKeyData.appendChild(doc.createTextNode(keyData));
            rootElement.appendChild(xmlKeyData);


            Element xmlFechaHora = doc.createElement("fechaHora");
            xmlFechaHora.appendChild(doc.createTextNode((DateUtil.getNowDate())));
            rootElement.appendChild(xmlFechaHora);

            Element xmlUser = doc.createElement("user");
            xmlUser.appendChild(doc.createTextNode("USRADCCE02"));
            rootElement.appendChild(xmlUser);


            res = getStringFromDocument(doc);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        }
        return res;

    }

    private String getStringFromDocument(Document doc) {
        try {
            DOMSource domSource = new DOMSource(doc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);
            return writer.toString();
        } catch (TransformerException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
