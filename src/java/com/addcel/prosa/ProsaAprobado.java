/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Salvador
 */
public class ProsaAprobado {
    private String bancoAdquiriente;
    private String nombreComercio;
    private String direccionComercio;
    private String afiliacion;
    private String track2;
    private String importe;
    private String moneda;
    private String fechaHora;
    private String claveOperacion;
    private String numeroCaja;
    private String producto;
    private String bancoEmisor;
    private String marca;
    private String poblacion;
    private String autorizacion;

    /**
     * @return the bancoAdquiriente
     */
    public String getBancoAdquiriente() {
        return bancoAdquiriente;
    }

    /**
     * @param bancoAdquiriente the bancoAdquiriente to set
     */
    public void setBancoAdquiriente(String bancoAdquiriente) {
        this.bancoAdquiriente = bancoAdquiriente;
    }

    /**
     * @return the nombreComercio
     */
    public String getNombreComercio() {
        return nombreComercio;
    }

    /**
     * @param nombreComercio the nombreComercio to set
     */
    public void setNombreComercio(String nombreComercio) {
        this.nombreComercio = nombreComercio;
    }

    /**
     * @return the afiliacion
     */
    public String getAfiliacion() {
        return afiliacion;
    }

    /**
     * @param afiliacion the afiliacion to set
     */
    public void setAfiliacion(String afiliacion) {
        this.afiliacion = afiliacion;
    }

    /**
     * @return the track2
     */
    public String getTrack2() {
        return track2;
    }

    /**
     * @param track2 the track2 to set
     */
    public void setTrack2(String track2) {
        this.track2 = track2;
    }

    /**
     * @return the importe
     */
    public String getImporte() {
        return importe;
    }

    /**
     * @param importe the importe to set
     */
    public void setImporte(String importe) {
        this.importe = importe;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the fechaHora
     */
    public String getFechaHora() {
        return fechaHora;
    }

    /**
     * @param fechaHora the fechaHora to set
     */
    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    /**
     * @return the claveOperacion
     */
    public String getClaveOperacion() {
        return claveOperacion;
    }

    /**
     * @param claveOperacion the claveOperacion to set
     */
    public void setClaveOperacion(String claveOperacion) {
        this.claveOperacion = claveOperacion;
    }

    /**
     * @return the numeroCaja
     */
    public String getNumeroCaja() {
        return numeroCaja;
    }

    /**
     * @param numeroCaja the numeroCaja to set
     */
    public void setNumeroCaja(String numeroCaja) {
        this.numeroCaja = numeroCaja;
    }

    /**
     * @return the producto
     */
    public String getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(String producto) {
        this.producto = producto;
    }

    /**
     * @return the bancoEmisor
     */
    public String getBancoEmisor() {
        return bancoEmisor;
    }

    /**
     * @param bancoEmisor the bancoEmisor to set
     */
    public void setBancoEmisor(String bancoEmisor) {
        this.bancoEmisor = bancoEmisor;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the autorizacion
     */
    public String getAutorizacion() {
        return autorizacion;
    }

    /**
     * @param autorizacion the autorizacion to set
     */
    public void setAutorizacion(String autorizacion) {
        this.autorizacion = autorizacion;
    }

    /**
     * @return the direccionComercio
     */
    public String getDireccionComercio() {
        return direccionComercio;
    }

    /**
     * @param direccionComercio the direccionComercio to set
     */
    public void setDireccionComercio(String direccionComercio) {
        this.direccionComercio = direccionComercio;
    }
    
    /**
     * @return the poblacion
     */
    public String getPoblacion() {
        return poblacion;
    }

    /**
     * @param poblacion the poblacion to set
     */
    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }
    
    public ProsaAprobado xmlToObject (String xml)
    {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        ProsaAprobado aprobado = new ProsaAprobado();
        
        try
        {
            xml = xml.replaceAll("xmlns=\"http://www.prosa.com.mx/xsd/\"", "xmlns:journal=\"http://www.w3.org/2001/XMLSchema-Instance\"");
            InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            InputSource inputSource = new InputSource(is);
            //NodeSet node = (NodeSet)(xpath.evaluate("/Transaccion",inputSource,XPathConstants.NODESET));
            NodeList nodes = (NodeList)(xpath.evaluate("//aprobada/claveOperacion",inputSource,XPathConstants.NODESET));
            
            
            for(int i=0; i<nodes.getLength(); i++)
            {
                Node thisNodes = nodes.item(i);
                NodeList moreNodes = thisNodes.getChildNodes();
                for(int j=0; j<moreNodes.getLength(); j++)                    
                {
                    Node node = moreNodes.item(j);
                    if(!node.getNodeName().equals("#text"))
                    {
                        String name = node.getNodeName();
                        if(name.equals("bancoAdquiriente"))
                            aprobado.setBancoAdquiriente(node.getTextContent().trim());
                        else if(name.equals("nombreComercio"))
                            aprobado.setNombreComercio(node.getTextContent().trim());
                        else if(name.equals("direccionComercio"))
                            aprobado.setDireccionComercio(node.getTextContent().trim());
                        else if(name.equals("afiliacion"))
                            aprobado.setAfiliacion(node.getTextContent().trim());
                        else if(name.equals("track2"))
                            aprobado.setTrack2(node.getTextContent().trim());
                        else if(name.equals("importe"))
                            aprobado.setImporte(node.getTextContent().trim());
                        else if(name.equals("moneda"))
                            aprobado.setMoneda(node.getTextContent().trim());
                        else if(name.equals("fechaHora"))
                            aprobado.setFechaHora(node.getTextContent().trim());
                        else if(name.equals("claveOperacion"))
                            aprobado.setClaveOperacion(node.getTextContent().trim());
                        else if(name.equals("numeroCaja"))
                            aprobado.setNumeroCaja(node.getTextContent().trim());
                        else if(name.equals("producto"))
                            aprobado.setProducto(node.getTextContent().trim());
                        else if(name.equals("bancoEmisor"))
                            aprobado.setBancoEmisor(node.getTextContent().trim());
                        else if(name.equals("marca"))
                            aprobado.setMarca(node.getTextContent().trim());
                        else if(name.equals("poblacion"))
                            aprobado.setPoblacion(node.getTextContent().trim());
                        else if(name.equals("autorizacion"))
                            aprobado.setAutorizacion(node.getTextContent().trim());                                
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return aprobado;
        
    }


    
}
