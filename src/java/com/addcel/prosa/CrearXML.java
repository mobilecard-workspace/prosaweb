/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

import com.prosa.mx.seguridad.DigestProsa;
import com.prosa.mx.seguridad.RsaProsa;
import java.io.StringWriter;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.pojoxml.core.PojoXml;
import org.pojoxml.core.PojoXmlFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author ADDCEL13
 */
public class CrearXML {
    
    
    
    public static String crear(ProsaDataTransaction prosaData) {
        PojoXml pojoxml = PojoXmlFactory.createPojoXml();
        String res = "";
        String keyData="";
        
        //Encripcion
        RsaProsa rsaProsa =new RsaProsa( );
        DigestProsa datos1 = new DigestProsa();
        try 
        {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Attr attr = null;

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("SeguridadProsa");
            attr = doc.createAttribute("xmlns");
            attr.setValue("http://www.prosa.com.mx/xsd/");
            rootElement.setAttributeNode(attr);
            doc.appendChild(rootElement);
            
            Element xmlKey = doc.createElement("key");
            xmlKey.appendChild(doc.createTextNode(CrearString.crea()));
            rootElement.appendChild(xmlKey);
            
            Element xmlKeyData = doc.createElement("keyData");
            String s = pojoxml.getXml(prosaData);
            s = s.replaceAll("<ProsaData>", "<Transaccion xmlns=\"http://www.prosa.com.mx/xsd/\"><ventaRequest>");
            s = s.replaceAll("</ProsaData>", "</ventaRequest></Transaccion>");
            System.out.println(s);

            try
            {
                keyData = datos1.getDigest( s.getBytes() );
                keyData = rsaProsa.byteToHex(rsaProsa.encrypt(keyData));
            }
            catch(Exception e)
            {
                keyData = "--";
            }
            
            
            
            xmlKeyData.appendChild(doc.createTextNode(keyData));
            rootElement.appendChild(xmlKeyData);
            
            
            Element xmlFechaHora = doc.createElement("fechaHora");
            xmlFechaHora.appendChild(doc.createTextNode( (new Date()).toString() ) );
            rootElement.appendChild(xmlFechaHora);
            
            Element xmlUser = doc.createElement("user");
            xmlUser.appendChild(doc.createTextNode("USRADCCE02") );
            rootElement.appendChild(xmlUser);


            res= getStringFromDocument(doc);
            // write the content into xml file
//            TransformerFactory transformerFactory = TransformerFactory.newInstance();
//            Transformer transformer = transformerFactory.newTransformer();
            //DOMSource source = new DOMSource(doc);
        //		StreamResult result = new StreamResult(new File("C:\\file.xml"));
        // 
        //		// Output to console for testing
        //		// StreamResult result = new StreamResult(System.out);
        // 
        //		transformer.transform(source, result);
        // 
        //		System.out.println("File saved!");

        } 
        
        catch (ParserConfigurationException pce) 
        {
            pce.printStackTrace();
        }
//       catch (TransformerException tfe) {
//            tfe.printStackTrace();
//        }     
        return res;
    }

    //method to convert Document to String
    private static String getStringFromDocument(Document doc)
    {
        try
        {
           DOMSource domSource = new DOMSource(doc);
           StringWriter writer = new StringWriter();
           StreamResult result = new StreamResult(writer);
           TransformerFactory tf = TransformerFactory.newInstance();
           Transformer transformer = tf.newTransformer();
           transformer.transform(domSource, result);
           return writer.toString();
        }
        catch(TransformerException ex)
        {
           ex.printStackTrace();
           return null;
        }
    }      
}
