/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

import com.addcel.tools.util.Constantes;
import javax.xml.rpc.ParameterMode;
import javax.xml.rpc.encoding.XMLType;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;

/**
 *
 * @author Salvador
 */
public class ProsaWS {

    //final String endpoint = "https://192.168.251.131:8080/Tx/services/Venta?wsdl";
//    final String endpoint = "http://localhost/WebSite1/serviciosWeb.asmx?wsdl";
    private String transaccion;
    private String seguridad;

//    Service service;
//    Call call;
    public ProsaWS(String transaccion, String seguridad) {
        this.transaccion = transaccion;
        this.seguridad = seguridad;
    }
    //https://192.168.251.131:8080/Tx/services/Reverso

    public String getReversa() {
        String ret = "";
        try {
            //Para QA Comentado, para evitar salidas por error
            // en produccion descomentar
            String endpoint = null;
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                endpoint = "http://192.168.251.131:8080/Tx/services/Reverso";
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                endpoint = "";
            }
            Service serviceWS = new Service();
            Call callWS = (Call) serviceWS.createCall();
            callWS.setTimeout((60 * 60 * 1000));
            callWS.setTargetEndpointAddress(new java.net.URL(endpoint));
            callWS.addParameter("seguridad", XMLType.XSD_STRING, ParameterMode.IN);
            callWS.addParameter("venta1", null, ParameterMode.IN);
            callWS.setReturnType(XMLType.XSD_STRING);
            //Solo para QA, VVLIRA 1 agosto 2013
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                ret = (String) callWS.invoke("reverso", new Object[]{seguridad, transaccion});
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                ret = "";
            }
            //termina solo para QA 1 agosto 2013
            //System.out.println( ret);  
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return ret;
    }

    public String getVenta() {
        String ret = "";
        try {
            //Para QA Comentado, para evitar salidas por error
            // en produccion descomentar
            String endpoint = null;
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                endpoint = "http://192.168.251.131:8080/Tx/services/Venta";
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                endpoint = "";
            }

            Service serviceWS = new Service();
            Call callWS = (Call) serviceWS.createCall();
            callWS.setTimeout((60 * 60 * 1000));
            callWS.setTargetEndpointAddress(new java.net.URL(endpoint));
            callWS.addParameter("seguridad", XMLType.XSD_STRING, ParameterMode.IN);
            callWS.addParameter("venta1", null, ParameterMode.IN);
            callWS.setReturnType(XMLType.XSD_STRING);

            //Solo para QA, VVLIRA 1 agosto 2013
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                ret = (String) callWS.invoke("venta", new Object[]{seguridad, transaccion});
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                ret = "";
            }
            //termina solo para QA 1 agosto 2013
            //System.out.println( ret);  
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return ret;
    }

    public String getDevolution() {
        String ret = "";
        try {
            //Para QA Comentado, para evitar salidas por error
            // en produccion descomentar
            String endpoint = null;
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                endpoint = "http://192.168.251.131:8080/Tx/services/Devolucion";
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                endpoint = "";
            }
            Service serviceWS = new Service();
            Call callWS = (Call) serviceWS.createCall();
            callWS.setTimeout((60 * 60 * 1000));
            callWS.setTargetEndpointAddress(new java.net.URL(endpoint));
            callWS.addParameter("seguridad", XMLType.XSD_STRING, ParameterMode.IN);
            callWS.addParameter("devolucion", null, ParameterMode.IN);
            callWS.setReturnType(XMLType.XSD_STRING);
            //Solo para QA, VVLIRA 1 agosto 2013
            if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_PROD)) {
                ret = (String) callWS.invoke("devolucion", new Object[]{seguridad, transaccion});
            } else if (Constantes.AMBIENTE_ACTUAL.equals(Constantes.AMBIENTE_QA)) {
                ret = "";
            }
            //termina solo para QA 1 agosto 2013
            //System.out.println( ret);  
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return ret;
    }
}
