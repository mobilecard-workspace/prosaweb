/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.prosa;

import com.addcel.tools.encryption.SHA1;
import com.prosa.mx.seguridad.RsaProsa;
import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ADDCEL13
 */
public class CrearString {
    
    public static String crea()
    {
        String user ="";
        String ip ="";
        String mac ="";
        String pwd ="";
        String date ="";
        String data = "";
        String sha1 = "";

        String s = "";
        RsaProsa rsaProsa =new RsaProsa( );


        //Step 1: Get data
        //user = "USRADCEL01"; PRUEBAS
        user="USRADCCE02";
        //pwd="44Dd<el1";  PRUEBAS
        pwd="ant4jai3";
        ip = "50.57.192.210";//getLocalIpAddress();
        mac = getLocalMacAddress();            
        date = getSpecialDateFormat();
        data =  ip + "|" + mac + "|" + pwd + "|" + date + "|";
        SHA1 sha1Encrypt = new SHA1();

        try
        {
            sha1 = sha1Encrypt.encrypt(data);
            data = data + sha1;
            System.out.println(data);
            data = rsaProsa.byteToHex(rsaProsa.encrypt(data));
        }
        
        catch(Exception e)
        {
            data = "--";
        }
        
        return data;
        
    }
    
    static private String getLocalIpAddress()
    {
        String ipAddress = "";
        try
        {
            ipAddress = Inet4Address.getLocalHost().getHostAddress();
        }
        catch(Exception e)
        {
            ipAddress = "";
        }
        return ipAddress;
    }
    
    static private String getLocalMacAddress()
    {
        String macAddress="";
        try
        {
            NetworkInterface ni = NetworkInterface.getByInetAddress(Inet4Address.getLocalHost());

            if (ni != null) {
                byte[] byteMac = ni.getHardwareAddress();
                if (byteMac != null) {
                    /*
                        * Extract each array of mac address and convert it 
                        * to hexa with the following format 
                        * 08-00-27-DC-4A-9E.
                        */
                    for (int i = 0; i < byteMac.length; i++) {
                        macAddress += String.format("%02X%s",
                                byteMac[i], (i < byteMac.length - 1) ? "-" : "");
                    }

                } 
                else 
                {
                    System.out.println("Address doesn't exist or is not " +
                            "accessible.");
                }
            } 
            else 
            {
                System.out.println("Network Interface for the specified " +
                        "address is not found.");
            }    
        }
        catch(Exception e)
        {
            macAddress="";            
        }
        return macAddress;
    }
    static private String getSpecialDateFormat()
    {        
        SimpleDateFormat formatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formathh = new SimpleDateFormat("hh");
        Date today = new Date();
        StringBuilder formatedDate = new StringBuilder(formatYYYYMMDD.format(today)+"T"+formathh.format(today));
        return formatedDate.toString();
    }    
    
}
