
package com.addcel.prosa;

import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Salvador
 */
@WebService(serviceName = "WsProsa")
public class WsProsa {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "echo")
    public String echo(@WebParam(name = "echoString") String echoString) {
        return "Echo: " + echoString + " !";
    }
    
    @WebMethod(operationName = "authorize")
    public ProsaResponse authorize(
                @WebParam(name = "card") String card,
                @WebParam(name = "vigencia") String vigencia,
                @WebParam(name = "cvv2") String cvv2,
                @WebParam(name = "monto") String monto,
                @WebParam(name = "nombre") String nombre,
                @WebParam(name = "afiliacion") String afiliacion
            ) 
    {
        ProsaTransaction transaction = new ProsaTransaction();
        
        System.out.println("*********************ECOMMERCE***********************\n");
        System.out.println("Sengin Auth...\n");
        System.out.println("card="+card);
        System.out.println("vigencia="+vigencia);
        System.out.println("cvv2="+cvv2);
        System.out.println("monto="+monto);
        System.out.println("nombre="+nombre);
        System.out.println("afiliacion="+afiliacion);
        
        return (new ProsaTransaction()).authorization(card, vigencia, cvv2, monto, nombre,afiliacion);
    }
    
    @WebMethod(operationName = "reversal")
    public ProsaResponse reversal(
                @WebParam(name = "transactionIdn") String transactionIdn
            ) 
    {
        //ProsaTransaction transaction = new ProsaTransaction();
        return (new ProsaTransaction()).reversal(transactionIdn);
    }
    
    @WebMethod(operationName = "devolution")
    public ProsaResponse devolution(
                @WebParam(name = "transactionIdn") String transactionIdn
            ) 
    {
        //ProsaTransaction transaction = new ProsaTransaction();
        return (new ProsaTransaction()).devolution(transactionIdn);
    }

}
