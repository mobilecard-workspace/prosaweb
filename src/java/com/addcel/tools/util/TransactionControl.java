/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.tools.util;

import com.addcel.common.MCUser;
import com.addcel.prosa.ProsaDataTransaction;
import com.addcel.prosa.ProsaDataDev;
import com.addcel.prosa.ProsaData;
import com.addcel.prosa.ProsaResponse;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import javax.naming.InitialContext;

/**
 *
 * @author ADDCEL13
 */
public class TransactionControl {
    
    
//    public static void updateTransaction(int transactionId,  ProsaResponse response)
//    {
//        TransactionControl.updateTransaction(transactionId,  response,"");
//    }
    
    public static void updateTransaction(int transactionId, ProsaResponse prosaResponse)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        InitialContext ic = null;
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Bloqueando tablas
            query = "LOCK TABLES TransaccionProsa WRITE";
            st.execute(query);            


            //Actualizando mensaje de AMex
            query = "UPDATE TransaccionProsa SET "
                    + "claveOperacion = '"+prosaResponse.getClaveOperacion()+"', "
                    + "autorizacion = '"+prosaResponse.getAutorizacion()+"', "
                    + "NombreComercio = '"+prosaResponse.getNombreComercio()+"', "
                    + "DireccionComercio = '"+prosaResponse.getDireccionComercio()+"', "
                    + "Producto = '"+prosaResponse.getProducto()+"', "
                    + "BancoEmisor = '"+prosaResponse.getBancoEmisor()+"', "
                    + "Marca = '"+prosaResponse.getMarca()+"', "
                    + "Poblacion = '"+prosaResponse.getPoblacion()+"', "
                    + "BancoAdquiriente = '"+prosaResponse.getBancoAdquirente()+"' "
                    +" WHERE TransaccionProsaIdn="+transactionId;
            System.out.println("Insert:"+query);
            st.executeUpdate(query);
            
            
            
            //Desloqueando tablas
            query = "UNLOCK TABLES";
            st.execute(query);            
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                //Desatorar tablas en caso de que se hayan quedado atoradas
                if(conn!=null && !conn.isClosed() && st!=null && !st.isClosed())
                {
                    st.execute("UNLOCK TABLES");                    
                }
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
                ds = new DatabaseImpl();
                conn = ds.getConnection();
                st = conn.createStatement();
                st.execute("UNLOCK TABLES");
                st.close();                    
                conn.close();
               
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        
    }
     
    
    public static void updateTransaction(int transactionId, String claveOperacion, String autorizacion)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        InitialContext ic = null;
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Bloqueando tablas
            query = "LOCK TABLES TransaccionProsa WRITE";
            st.execute(query);            


            //Actualizando mensaje de AMex
            query = "UPDATE TransaccionProsa SET claveOperacion = '"+claveOperacion+"', "
                    + "autorizacion = '"+autorizacion+"' WHERE TransaccionProsaIdn="+transactionId;
            System.out.println("Insert:"+query);
            st.executeUpdate(query);
            
            
            //Desloqueando tablas
            query = "UNLOCK TABLES";
            st.execute(query);            
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                //Desatorar tablas en caso de que se hayan quedado atoradas
                if(conn!=null && !conn.isClosed() && st!=null && !st.isClosed())
                {
                    st.execute("UNLOCK TABLES");                    
                }
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
                ds = new DatabaseImpl();
                conn = ds.getConnection();
                st = conn.createStatement();
                st.execute("UNLOCK TABLES");
                st.close();                    
                conn.close();
               
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        
    }
    
    public static void updateReversal(long originalTransactionId, long reversalTransactionId)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        InitialContext ic = null;
        int lastError = 0;
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();

            //Bloqueando tablas
            query = "LOCK TABLES TransaccionProsa WRITE";
            st.execute(query);            


            //Actualizando mensaje de AMex
            query = "UPDATE TransaccionProsa SET TransaccionProsaReversa='S', TransaccionProsaReversaIdn="+reversalTransactionId
                    +" WHERE TransaccionProsaIdn="+originalTransactionId;
            System.out.println("Update:"+query);
            st.executeUpdate(query);
            
            //Desloqueando tablas
            query = "UNLOCK TABLES";
            st.execute(query);            
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                //Desatorar tablas en caso de que se hayan quedado atoradas
                if(conn!=null && !conn.isClosed() && st!=null && !st.isClosed())
                {
                    st.execute("UNLOCK TABLES");                    
                }
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
                ds = new DatabaseImpl();
                conn = ds.getConnection();
                st = conn.createStatement();
                st.execute("UNLOCK TABLES");
                st.close();                    
                conn.close();
               
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        
    }
    
    
    public synchronized static int createTransaction(ProsaDataTransaction prosaData)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        InitialContext ic = null;
        int transactionId=0;        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();
            //int lastIdn = 0;

            //Bloqueando tablas
            transactionId=getSecuencia();
            query = "LOCK TABLES TransaccionProsa WRITE";
            st.execute(query);            

            //Ejecutando Queries

            //Obteniendo ultimo id
            //query = "SELECT ifnull(max(TransaccionProsaIdn),0) FROM TransaccionProsa";
            //rs = st.executeQuery(query);
            //if(rs.next())
            //{
            //    lastIdn = rs.getInt(1);
            //}

            //lastIdn++;
            //transactionId = lastIdn;
            //Insertando datos
            query = "INSERT INTO TransaccionProsa (TransaccionProsaIdn, modoEntrada, capacidadPin,conditionCode,"+
                    "capacidadTerminal,ubicacionTerminal, importe, track2, "
                    + "cvv2,fechaHora, fechaAplicacion,afiliacion,"
                    + "numeroCaja,moneda,claveVenta,operador,"
                    + "nombre,terminal,fiid,indMedioAcceso)"+
                    " VALUES "+
                    "(" + transactionId + ",'" + prosaData.getModoEntrada() + "','"+prosaData.getCapacidadPin()+"','" + prosaData.getConditionCode()+ 
                    "','" + prosaData.getCapacidadTerminal() + "','" + prosaData.getUbicacionTerminal() + "','" + prosaData.getImporte() + "','" + prosaData.getTrack2() + 
                    "','" + prosaData.getCvv2()+ "','" + prosaData.getFechaHora()+ "','" + prosaData.getFechaAplicacion() + "','" + prosaData.getAfiliacion() + 
                    "','" + prosaData.getNumeroCaja() + "','" + prosaData.getMoneda() + "','" + prosaData.getClaveVenta() + "','" + prosaData.getOperador() + 
                    "','" + prosaData.getNombre() + "','"+ prosaData.getTerminal() + "','" + prosaData.getFiid() + "','"+ prosaData.getIndMedioAcceso() +"')";
            System.out.println("Insert:"+query);
            st.executeUpdate(query);


            //Desloqueando tablas
            query = "UNLOCK TABLES";
            st.execute(query);            
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                //Desatorar tablas en caso de que se hayan quedado atoradas
                if(conn!=null && !conn.isClosed() && st!=null && !st.isClosed())
                {
                    st.execute("UNLOCK TABLES");                    
                }
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
                ds = new DatabaseImpl();
                conn = ds.getConnection();
                st = conn.createStatement();
                st.execute("UNLOCK TABLES");
                st.close();                    
                conn.close();
               
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        return transactionId;
    }
    
    
    
    public static int createDevolution(ProsaDataDev prosaData, ProsaResponse prosaResponse,String transactionIdn)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        InitialContext ic = null;
        int transactionId=0;
                
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();
            //int lastIdn = 0;

            //Bloqueando tablas
            transactionId = getSecuenciaReverso();
            query = "LOCK TABLES DevolucionProsa WRITE";
            st.execute(query);            

            
            //Ejecutando Queries

            //Obteniendo ultimo id
            //query = "SELECT ifnull(max(DevolucionProsaIdn),0) FROM DevolucionProsa";
            //rs = st.executeQuery(query);
            //if(rs.next())
            //{
            //    lastIdn = rs.getInt(1);
            //}

            //lastIdn++;
            

            //Insertando datos
            query = "INSERT INTO DevolucionProsa " 
                    + "(DevolucionProsaIdn, DevolucionProsaModoEntrada, DevolucionProsaCapacidadPin,DevolucionProsaCapacidadTerminal,"
                    + "DevolucionProsaUbicacionTerminal,DevolucionProsaImporte, DevolucionProsaTrack2, DevolucionProsaCvv, "
                    + "DevolucionProsaFechaHora, DevolucionProsaFechaAplicacion, DevolucionProsaAfiliacion,DevolucionProsaNumeroCajas,"
                    + "DevolucionProsaMoneda,DevolucionProsaClaveVenta,DevolucionProsaOperador,DevolucionProsaNombre,"
                    + "DevolucionProsaTerminal,DevolucionProsaFiid, DevolucionProsaClaveOperacion, DevolucionProsaProducto, DevolucionProsaBancoEmisor,"
                    + "DevolucionProsaMarca, DevolucionProsaAutorizacion)"+
                    " VALUES "+
                    "(" + transactionId + ",'" + prosaData.getModoEntrada() + "','"+prosaData.getCapacidadPin()+"','" + prosaData.getCapacidadTerminal()+ 
                    "','" + prosaData.getUbicacionTerminal() + "','" + prosaData.getImporte() + "','" + prosaData.getTrack2() + "','" + prosaData.getCvv2() + 
                    "','" + prosaData.getFechaHora() +  "','" + prosaData.getFechaAplicacion()+ "','" + prosaData.getAfiliacion() + "','" + prosaData.getNumeroCaja() + 
                    "','" + prosaData.getMoneda() + "','" + prosaData.getClaveVenta() + "','" + prosaData.getOperador() + "','" + prosaData.getNombre() + 
                    "','" + prosaData.getTerminal() + "','"+ prosaData.getFiid() + "','"+prosaResponse.getClaveOperacion()+"','"+prosaResponse.getProducto()+
                    "','"+prosaResponse.getBancoEmisor() +"','"+prosaResponse.getMarca()+"','"+prosaResponse.getAutorizacion()+"')";
            System.out.println("Insert:"+query);
            st.executeUpdate(query);
              

            //Desloqueando tablas
            query = "UNLOCK TABLES";
            st.execute(query);            
            
            //Bloqueando tablas
            query = "LOCK TABLES TransaccionProsa WRITE";
            st.execute(query);            

            query = "UPDATE TransaccionProsa SET TransaccionProsaDevolucionIdn =  " + transactionId + " Where TransaccionProsaIdn = "+transactionIdn;
            System.out.println("Update:"+query);
            st.executeUpdate(query);

            //Desloqueando tablas
            query = "UNLOCK TABLES";
            st.execute(query);            
            
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                //Desatorar tablas en caso de que se hayan quedado atoradas
                if(conn!=null && !conn.isClosed() && st!=null && !st.isClosed())
                {
                    st.execute("UNLOCK TABLES");                    
                }
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
                ds = new DatabaseImpl();
                conn = ds.getConnection();
                st = conn.createStatement();
                st.execute("UNLOCK TABLES");
                st.close();                    
                conn.close();
               
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                }
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        return transactionId;
    }
    
    
    public static ProsaData getData(String transactionIdn)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        ProsaData data = new ProsaData();
        
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();


            //Obteniendo ultimo id
            query = "SELECT TransaccionProsaIdn, modoEntrada, capacidadPin,conditionCode,"+
                    "capacidadTerminal,ubicacionTerminal, importe, track2, " +
                    "fechaHora, fechaAplicacion,afiliacion," +
                    "numeroCaja,moneda,claveVenta,operador," +
                    "nombre,terminal,fiid,indMedioAcceso,claveOperacion, autorizacion,cvv2  "
                    + "FROM TransaccionProsa WHERE TransaccionProsaIdn="+transactionIdn;
            rs = st.executeQuery(query);
            if(rs.next())
            {
                data.setIdn(rs.getString(1)); 
                data.setModoEntrada(rs.getString(2)); 
                data.setCapacidadPin(rs.getString(3)); 
                data.setConditionCode(rs.getString(4)); 
                data.setCapacidadTerminal(rs.getString(5)); 
                data.setUbicacionTerminal(rs.getString(6)); 
                data.setImporte(rs.getString(7)); 
                data._setTrack2(rs.getString(8)); 
                data.setFechaHora(rs.getString(9)); 
                data.setFechaAplicacion(rs.getString(10)); 
                data.setAfiliacion(rs.getString(11)); 
                data.setNumeroCaja(rs.getString(12)); 
                data.setMoneda(rs.getString(13)); 
                data.setClaveVenta(rs.getString(14)); 
                data.setOperador(rs.getString(15)); 
                data.setNombre(rs.getString(16)); 
                data.setTerminal(rs.getString(17)); 
                data.setFiid(rs.getString(18)); 
                data.setIndMedioAcceso(rs.getString(19)); 
                data.setClaveOperacion(rs.getString(20)); 
                data.setAutorizacion(rs.getString(21)); 
                data._setCvv2(rs.getString(22));
            }
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        return data;
    
    }
    
    
    public static Boolean getUser(String user, String password)
    {
        DatabaseImpl ds = new DatabaseImpl();
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        Boolean res = false;
        
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();


            //Obteniendo ultimo id
            query = "SELECT UsersProsaLogin FROM UsersProsa "
                    + "WHERE UsersProsaLogin='"+user+"' and UsersProsaPassword='"+password+"'";
            rs = st.executeQuery(query);
            if(rs.next())
                res = true;
            else
                res = false;
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        return res;
    
    }
    
    public static MCUser getMCUserData(String userId)
    {
        DatabaseImpl ds = new DatabaseImpl("mobilecard");
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        String query = "";
        MCUser mobileCardUser = null;
        
        
        try
        {
            conn = ds.getConnection();
            st = conn.createStatement();


            //Obteniendo ultimo id
            query = "select id_usuario,usr_login,usr_nombre, usr_apellido, usr_materno, usr_tdc_numero, usr_tdc_vigencia, desc_tipo_tarjeta "
                    + "from t_usuarios left join t_tipo_tarjeta on t_usuarios.id_tipo_tarjeta=t_tipo_tarjeta.id_tipo_tarjeta "
                    + "where id_usr_status = 1 and usr_login='"+userId+"'";
            rs = st.executeQuery(query);
            System.out.println(query);
            if(rs.next())
            {
                mobileCardUser =  new MCUser();
                mobileCardUser.setUserId(rs.getString(1));
                mobileCardUser.setUserLogin(rs.getString(2));
                mobileCardUser.setUserNombre(rs.getString(3));
                mobileCardUser.setUserApellido(rs.getString(4));
                //mobileCardUser.setUserId(rs.getString(5)); Apellido materno
                mobileCardUser.setUserTdc(rs.getString(6));
                mobileCardUser.setUserVig(rs.getString(7));
                mobileCardUser.setUserTipoTarjeta(rs.getString(8));
                
            }
        }
        catch(Exception e)
        {
            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
        }
        finally
        {
            try
            {
                
                //Statement
                if(st!=null && !st.isClosed())
                {
                    st.close();
                    st = null;
                }
                if(st!=null)
                {
                    st = null;
                }
                
                //Connection
                if(conn!=null && !conn.isClosed())
                {
                    conn.close();
                    conn = null;
                }
                if(conn!=null)
                {
                    conn = null;
                }
                
            }
            catch(Exception e)
            {
                try
                {
                    if(conn!=null && !conn.isClosed())
                    {
                        conn.close();
                        conn = null;
                    }
                    if(conn!=null)
                    {
                        conn = null;
                    }                    
                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
                }
                catch(Exception ex)
                {
                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
                }
            }
        }
        return mobileCardUser;
    
    }
    
//    public static AmexData getData(long transactionIdn)
//    {
//        DatabaseImpl ds = new DatabaseImpl();
//        Connection conn = null;
//        Statement st = null;
//        ResultSet rs = null;
//        String query = "";
//        AmexData data = new AmexData();
//        
//        
//        try
//        {
//            conn = ds.getConnection();
//            st = conn.createStatement();
//
//
//            //Obteniendo ultimo id
//            query = "SELECT transaccionLocalDateTime,transaccionMonto, transaccionTarjeta,transaccionExpira, transaccionCid,transaccionTipo,"
//                    + "transaccionAccReference  FROM Transaccion WHERE transaccionIdn="+transactionIdn;
//            rs = st.executeQuery(query);
//            if(rs.next())
//            {
//                data.setLocalTransactionDateAndTime(rs.getString(1)); 
//                data.setAmount(rs.getString(2)); 
//                data.setPrimaryAccountNumber(rs.getString(3)); 
//                data.setCardExpirationDate(rs.getString(4)); 
//                data.setCid(rs.getString(5)); 
//                data.setMessageType(rs.getString(6)); 
//                data.setAccReferenceData(rs.getString(7)); 
//                data.setSystemTraceAuditNumber(systemTraceAuditNumber);
//                
//                
//            }
//        }
//        catch(Exception e)
//        {
//            System.out.println("ECOMMERCE POOL: General Exception:"+ e.getMessage());            
//        }
//        finally
//        {
//            try
//            {
//                
//                //Statement
//                if(st!=null && st.isClosed())
//                {
//                    st.close();
//                    st = null;
//                }
//                if(st!=null)
//                {
//                    st = null;
//                }
//                
//                //Connection
//                if(conn!=null && conn.isClosed())
//                {
//                    conn.close();
//                    conn = null;
//                }
//                if(conn!=null)
//                {
//                    conn = null;
//                }
//                
//            }
//            catch(Exception e)
//            {
//                try
//                {
//                    if(conn!=null && conn.isClosed())
//                    {
//                        conn.close();
//                        conn = null;
//                    }
//                    if(conn!=null)
//                    {
//                        conn = null;
//                    }                    
//                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
//                }
//                catch(Exception ex)
//                {
//                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
//                }
//            }
//        }
//        return data;
//    
//    }
//    
//    public static void setBitacora()
//    {
//        DatabaseImpl ds = new DatabaseImpl();
//        Connection conn = null;
//        Statement st = null;
//        ResultSet rs = null;
//        String query = "";
//        InitialContext ic = null;
//        
//        try
//        {
//            conn = ds.getConnection();
//            st = conn.createStatement();
//            int lastIdn = 0;
//
//            //Bloqueando tablas
//            query = "LOCK TABLES Bitacora WRITE";
//            st.execute(query);            
//
//            //Ejecutando Queries
//
//            //Obteniendo ultimo id
//            query = "SELECT ifnull(max(BitacoraIdn),0) FROM Bitacora";
//            rs = st.executeQuery(query);
//            if(rs.next())
//            {
//                lastIdn = rs.getInt(1);
//            }
//
//            lastIdn++;
//
//            //Insertando datos
////            query = "INSERT INTO Bitacora (BitacoraIdn, SistemaIdn, BitacoraTarjeta,BitacoraNoTarjeta,"+
////                    "EstadoId, BitacoraTipo, BitacoraDsc, transaccionIdn)"+
////                    " VALUES "+
////                    "(" +lastIdn + "," + sistema + ",'" + tarjeta + "','" + noTarjeta + "'" +
////                    ",'" + estado + "','" + tipo + "','"+infoAdicional+"',"+transaccion+")";
//            System.out.println("Insert:"+query);
//            st.executeUpdate(query);
//
//
//            //Desloqueando tablas
//            query = "UNLOCK TABLES";
//            st.execute(query);            
//        }
//        catch(Exception e)
//        {
//            
//        }
//        finally
//        {
//            try
//            {
//                //Desatorar tablas en caso de que se hayan quedado atoradas
//                if(conn!=null && !conn.isClosed() && st!=null && !st.isClosed())
//                {
//                    st.execute("UNLOCK TABLES");                    
//                }
//                
//                //Statement
//                if(st!=null && st.isClosed())
//                {
//                    st.close();
//                    st = null;
//                }
//                if(st!=null)
//                {
//                    st = null;
//                }
//                
//                //Connection
//                if(conn!=null && conn.isClosed())
//                {
//                    conn.close();
//                    conn = null;
//                }
//                if(conn!=null)
//                {
//                    conn = null;
//                }
//                
//                ds = new DatabaseImpl();
//                conn = ds.getConnection();
//                st = conn.createStatement();
//                st.execute("UNLOCK TABLES");
//                st.close();                    
//                conn.close();
//               
//                if(conn!=null && !conn.isClosed())
//                {
//                    conn.close();
//                }
//            }
//            catch(Exception e)
//            {
//                try
//                {
//                    if(conn!=null && conn.isClosed())
//                    {
//                        conn.close();
//                        conn = null;
//                    }
//                    if(conn!=null)
//                    {
//                        conn = null;
//                    }                    
//                    System.out.println("ECOMMERCE POOL: Error interno al intentar desbloquear tablas:"+e.getMessage());
//                }
//                catch(Exception ex)
//                {
//                    System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());                        
//                }
//            }
//        }
//        
//        //return transactionId;
//    }
    public static synchronized int getSecuencia(){
        DatabaseImpl ds = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String query = "";
        int lastId=0;
        try{
            ds = new DatabaseImpl("mobilecard");
            query="select mobilecard.get_next_value(\"@SECUENCIA_PROSA\")";
            conn=ds.getConnection();
            pst=conn.prepareStatement(query);
            rs=pst.executeQuery();
            if(rs.next()){
                lastId=rs.getInt(1);
            }
        }catch(Exception ex){
            System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());        
        }
        return lastId;
    }
    public static synchronized int getSecuenciaReverso(){
        DatabaseImpl ds = null;
        Connection conn = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String query = "";
        int lastId=0;
        try{
            ds = new DatabaseImpl("mobilecard");
            query="select mobilecard.get_next_value(\"@SECUENCIA_PROSA_REVERSO\")";
            conn=ds.getConnection();
            pst=conn.prepareStatement(query);
            rs=pst.executeQuery();
            if(rs.next()){
                lastId=rs.getInt(1);
            }
        }catch(Exception ex){
            System.out.println("ECOMMERCE POOL: Error interno de base de datos:"+ex.getMessage());        
        }finally{
            if(rs!=null){
                try{
                rs.close();
                }catch(Exception ex){ System.out.println("ECOMMERCE POOL: Error interno de base de datos al cerrar RS:"+ex.getMessage());};
            }
            if(pst!=null){
                try{
                    pst.close();
                }catch(Exception ex){ System.out.println("ECOMMERCE POOL: Error interno de base de datos al cerrar PST:"+ex.getMessage());};
            }
            if(conn!=null){
                try{
                    conn.close();
                }catch(Exception ex){ System.out.println("ECOMMERCE POOL: Error interno de base de datos al cerrar CONN:"+ex.getMessage());};
            }
        }
        return lastId;
    }
    
}
