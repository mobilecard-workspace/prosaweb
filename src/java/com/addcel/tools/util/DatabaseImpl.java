/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.tools.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ADDCEL13
 */
public class DatabaseImpl {
    private Connection  connection  = null;
//    private String userName = "mobilcard";//root-mobilcard
//    private String password = "Mobil1696Card";// 54V173?Fo0nt-mobilcard
//    private String url = "jdbc:mysql://DataBaseMC/mobilcard";//addcel-mobilcard
    
    private String userName = "ecommerce";//root-mobilcard
    private String password = "AddcellWeb2012";// 54V173?Fo0nt-mobilcard
    private String url = "jdbc:mysql://DataBaseMC/ecommerce";//addcel-mobilcard

    
//    private String userName = "root";//root-mobilcard
//    private String password = "sistemas";// 54V173?Fo0nt-mobilcard
//    private String url = "jdbc:mysql://localhost/ecommerce";//addcel-mobilcard

    public DatabaseImpl()
    {
        
    }
    
    public DatabaseImpl(String dataBase)
    {
        if(dataBase.equals("mobilecard"))
        {
            userName = "mobilcard";
            password="Mobil1696Card";
            url = "jdbc:mysql://DataBaseMC/"+dataBase;
        }
    }
    
    public  Connection getConnection() throws ClassNotFoundException, SQLException{
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection(url, userName, password);
        return connection;
    }

    public void disconect() throws SQLException{
        connection.close();
    }    
    
    public List executeQuery(String query)
    {
        List tabla = new ArrayList();
        int size = 0;
        try {
            PreparedStatement statement;
            ResultSet resultSet = null;
            statement = getConnection().prepareStatement(query);
            resultSet = statement.executeQuery();
            if(resultSet.next())
            {
        	while(resultSet.next())
                {
                    size++;
        	}                
                resultSet.first();
                
        	while(resultSet.next())
                {
                    List linea = new ArrayList();
                    for(int i=0; i<size ; i++)
                    {
                        linea.add(resultSet.getString(i));
                    }
                    tabla.add(linea);
        	}
            }
        } catch (SQLException ex) {
            System.out.println("AMEX Error:"+ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("AMEX Error:"+ex.getMessage());
        }finally {
            try
            {
                disconect();
            }
            catch(Exception ee)
            {
            System.out.println("AMEX Error:"+ee.getMessage());
            }
        }
        return  tabla; 
    }
    
    public String executeScalar(String query)
    {
        List tabla = new ArrayList();
        String resultado = "";
        int size = 0;
        try {
            PreparedStatement statement;
            ResultSet resultSet = null;
            statement = getConnection().prepareStatement(query);
            resultSet = statement.executeQuery();
            if(resultSet.next())
            {
        	while(resultSet.next())
                {
                    size++;
        	}                
                resultSet.first();
                
                if(size==1)
                {
                    while(resultSet.next())
                    {
                        resultado=resultSet.getString(0);
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("AMEX Error:"+ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("AMEX Error:"+ex.getMessage());
        }finally {
            try
            {
                disconect();
            }
            catch(Exception ee)
            {
            System.out.println("AMEX Error:"+ee.getMessage());
            }
        }
        return resultado; 
    }
    
}
