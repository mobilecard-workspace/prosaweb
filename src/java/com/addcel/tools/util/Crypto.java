package com.addcel.tools.util;

import org.bouncycastle.crypto.digests.SHA1Digest;
import sun.security.provider.MD5;


public class Crypto {
        final static private String seed = "1234567890ABCDEF0123456789ABCDEF";	

	public static String aesEncrypt(String cleartext) {
		String encryptedText;
                
		try{
			return AESBinFormat.encode(cleartext, Crypto.seed);
		}catch(Exception e){
			encryptedText = "";
		}
		
		return encryptedText;
	}
        
	public static String aesEncrypt2(String cleartext, String seed) {
		String encryptedText;
                String thisSeed="";
		
                if(seed==null || seed.equals(""))
                    thisSeed = Crypto.seed;

                
		try{
                    System.out.println("Seed:"+thisSeed);
			return AESBinFormat.encode(cleartext, thisSeed);
		}catch(Exception e){
                    e.printStackTrace();
			encryptedText = "";
		}
		
		return encryptedText;
	}
        
	public static String aesEncrypt(String cleartext, String seed) {
		String encryptedText;
                String thisSeed="";
		
                if(seed==null || seed.equals(""))
                    thisSeed = Crypto.seed;

                
		try{
			return AESBinFormat.encode(replaceConAcento(cleartext), thisSeed);
		}catch(Exception e){
                    e.printStackTrace();
			encryptedText = "";
		}
		
		return encryptedText;
	}
	
	
	public static String aesDecrypt(String encrypted,String seed) {
		String decryptedText;
                String thisSeed="";
		
                if(seed==null || seed.equals(""))
                    thisSeed = Crypto.seed;
                else
                    thisSeed = parsePass(seed);
		
		try{
			return replaceHTMLAcento(AESBinFormat.decode(encrypted, thisSeed));
		}catch(Exception e){
			decryptedText = "";
		}
		
		return decryptedText;
	}
        
        public static String parsePass(String pass){
            int len = pass.length();
            String key = "";

            for (int i =0; i < 32 /len; i++){
                key += pass;
            }

            int carry = 0;
            while (key.length() < 32){
                key += pass.charAt(carry);
                carry++;
            }
            return key;
        }
        
        
        
//    public static String sha1(String s) {
//            if (s != null){
//                try {
//                    SHA1Digest digest = new SHA1Digest();
//                    byte[] bb = s.getBytes();
//                    digest.update(bb, 0, bb.length);
//
//                    byte[] digestValue = new byte[digest.getDigestSize()];
//                    digest.doFinal(digestValue, 0);
//
//                    return MD5.toHex(digestValue);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            return "";
//        }
//    
         public static String replaceHTMLAcento(String text){
        
           String Res = text.replace ("&Ntilde;","Ñ");
                  Res = Res.replace ("&ntilde;","ñ");
                  Res = Res.replace ("&Aacute;","Á");
                  Res = Res.replace ("&aacute;","á");
                  Res = Res.replace ("&Eacute;","É");
                  Res = Res.replace ("&eacute;","é");
                  Res = Res.replace ("&Iacute;","Í");
                  Res = Res.replace ("&iacute;","í");
                  Res = Res.replace ("&Oacute;","Ó");
                  Res = Res.replace ("&oacute;","ó");
                  Res = Res.replace ("&Uacute;","Ú");
                  Res = Res.replace ("&uacute;","ú");
           
        return Res.toString();
    }
        

    
    public static String replaceConAcento(String text)
    {
        text = text.replaceAll("Ñ", "&Ntilde;");
        text = text.replaceAll("ñ", "&ntilde;");
        text = text.replaceAll("Á", "&Aacute;");
        text = text.replaceAll("á", "&aacute;");
        text = text.replaceAll("É", "&Eacute;");
        text = text.replaceAll("é", "&eacute;");
        text = text.replaceAll("Í", "&Iacute;");
        text = text.replaceAll("í", "&iacute;");
        text = text.replaceAll("Ó", "&Oacute;");
        text = text.replaceAll("ó", "&oacute;");
        text = text.replaceAll("Ú", "&Uacute;");
        text = text.replaceAll("ú", "&uacute;");

        /*
        StringBuffer sBuffer = new StringBuffer();
        for(int i=0; i<text.length(); i++){
            if(text.charAt(i)=='Ñ')
                sBuffer.append("&Ntilde;");
            else if(text.charAt(i)=='ñ')
                sBuffer.append("&ntilde;");
            else if(text.charAt(i)=='Á')
                sBuffer.append("&Aacute;");
            else if(text.charAt(i)=='á')
                sBuffer.append("&aacute;");
            else if(text.charAt(i)=='É')
                sBuffer.append("&Eacute;");
            else if(text.charAt(i)=='é')
                sBuffer.append("&eacute;");
            else if(text.charAt(i)=='Í')
                sBuffer.append("&Iacute;");
            else if(text.charAt(i)=='í')
                sBuffer.append("&iacute;");
            else if(text.charAt(i)=='Ó')
                sBuffer.append("&Oacute;");
            else if(text.charAt(i)=='ó')
                sBuffer.append("&oacute;");
            else if(text.charAt(i)=='Ú')
                sBuffer.append("&Uacute;");
            else if(text.charAt(i)=='ú')
                sBuffer.append("&uacute;");
            else
                sBuffer.append(text.charAt(i));           
        }
        * */
        
        return text;//sBuffer.toString();
    }

        
}
//  http://201.161.23.42:42422/WSAddcelSMS/WSAddcel.asmx?wsdl