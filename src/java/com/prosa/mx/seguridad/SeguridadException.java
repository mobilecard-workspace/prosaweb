package com.prosa.mx.seguridad;

/**
 * Created by IntelliJ IDEA.
 * User: ggerdoc
 * Date: Oct 10, 2005
 * Time: 11:06:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class SeguridadException extends Exception{
    /**
	 * 
	 */
	private static final long serialVersionUID = 6782304060110165775L;

	public SeguridadException(String message) {
        super(message);
    }
}
