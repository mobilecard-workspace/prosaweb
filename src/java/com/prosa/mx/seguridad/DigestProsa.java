package com.prosa.mx.seguridad;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
public class DigestProsa
{
    private final String	digits = "0123456789abcdef";

    public String getDigest ( byte[] buffer) throws SeguridadException
    {
        try
        {
            MessageDigest algorithm = MessageDigest.getInstance( "SHA-1" );
            algorithm.reset( );
            algorithm.update( buffer );
            byte[] digest = algorithm.digest( );
	    String txtDigest = byteToHex( digest );
            return txtDigest;
        }
        catch( NoSuchAlgorithmException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
    }
    
    private String byteToHex( final byte[] data ) throws SeguridadException
    {
        if( data == null )
        {
            throw new SeguridadException( "Data is null" );
        }
        final StringBuffer stringBuffer = new StringBuffer( );
        for (int i = 0; i < data.length; i++)
        {
            int	v = data[ i ] & 0xff;
            stringBuffer.append( digits.charAt( v >> 4 ) );
            stringBuffer.append( digits.charAt( v & 0xf ) );
        }
        return stringBuffer.toString( );
    }
}