package com.prosa.mx.seguridad;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class RsaProsa
{
    private final String	digits = "0123456789abcdef";
    private PublicKey publicKey;
    private Cipher cipher;
    private String path;
    public final static int PUBLIC   = 1;
    public final static int ENCRYPT  = 1;
    public final static int DECRYPT  = 2;

    public int getOpcion() {
        return opcion;
    }

    public void setOpcion(int opcion) {
        this.opcion = opcion;
    }

    private int opcion;

    public RsaProsa()
    {
        Security.addProvider( new BouncyCastleProvider( ) );
        publicKey = null;
        cipher = null;
        path = null;
        opcion = 0;
        //setFile( "C:/prod/certi" );        
        setFile( "/home/rmunoz/ProsaKey" );        
    }

    public String decrypt( final String data )throws SeguridadException
    {
        try
        {
            if( path == null )
            {
                throw new SeguridadException( "Path is null" );
            }
            load( RsaProsa.PUBLIC , RsaProsa.DECRYPT );
            final byte bytes [ ]= stringToByte( data );
            final String res = byteToString( cipher.doFinal( bytes ) );
            return res;
        }
        catch ( BadPaddingException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
        catch ( IllegalBlockSizeException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
    }


    public void load( final int opcion , final int modo ) throws SeguridadException
    {
        try
        {
            if( cipher != null )
            {
                return;
            }
            loadPublicKey( path );

            cipher = Cipher.getInstance( "RSA/ECB/PKCS1Padding" );
            switch( modo )
            {
                case DECRYPT:
                        loadPublicKey( path );
                        cipher.init( Cipher.DECRYPT_MODE , publicKey );
                    break;
                case ENCRYPT:
                        cipher.init( Cipher.ENCRYPT_MODE , publicKey );
                    break;
                default:
                    throw new SeguridadException( "No existe opcion" );
            }
        }
        catch (NoSuchAlgorithmException ex)
        {
            throw new SeguridadException( ex.toString( ) );
        }
        catch (NoSuchPaddingException ex)
        {
            throw new SeguridadException( ex.toString( ) );
        }
        catch (InvalidKeyException ex)
        {
            throw new SeguridadException( ex.toString( ) );
        }
    }

    public String encrypt( final String data )throws SeguridadException
    {
        try
        {
            if( path == null )
            {
                throw new SeguridadException( "Path is null" );
            }
            load( RsaProsa.PUBLIC , RsaProsa.ENCRYPT );
            final byte bytes [ ]= stringToByte( data );
            final byte bytesRes [ ]= cipher.doFinal( bytes );
            final String res = byteToString( bytesRes );
            return res;
        }
        catch ( BadPaddingException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
        catch ( IllegalBlockSizeException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
    }

    private void loadPublicKey( final String path ) throws SeguridadException
    {
        try
        {
            final byte bytes[] =  load( path + "/PublicKey.dat" );
            X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec( bytes );
            KeyFactory keyFactory = KeyFactory.getInstance( "RSA" , "BC" );
            publicKey  = keyFactory.generatePublic( x509EncodedKeySpec );
        }
        catch ( NoSuchAlgorithmException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
        catch ( NoSuchProviderException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
        catch ( InvalidKeySpecException ex )
        {
            throw new SeguridadException( ex.toString( ) );
        }
    }

    private String byteToString( final byte bytes[ ] ) throws SeguridadException
    {
        if( bytes == null )
        {
            throw new SeguridadException( "Bytes is null" );
        }

        char chars[] = new char[ bytes.length ];
        for( int i = 0; i < chars.length; i++ )
        {
            chars[ i ] = (char)(bytes [ i ] & 0xff);
        }
        final String res = String.valueOf( chars );
        return res;
    }

    private byte[ ] stringToByte( final String string ) throws SeguridadException
    {
        if( string == null )
        {
            throw new SeguridadException( "string is null" );
        }
        final char [] chars = string.toCharArray( );
        final byte res[] = new byte[ chars.length ];
        for( int i=0; i < chars.length; i++)
        {
            res[ i ] = (byte)chars[ i ];
        }
        return res;
    }

    private byte[] load( final String file ) throws SeguridadException
    {
        try
        {
            FileInputStream fileInputStream = null;
            fileInputStream = new FileInputStream( file );
            byte[] bytes = new byte[ fileInputStream.available( ) ];
		    fileInputStream.read( bytes );
		    fileInputStream.close( );
            return bytes;
        }
        catch ( FileNotFoundException ex )
        {
            throw new  SeguridadException( ex.toString( ) );
        }
        catch ( IOException ex )
        {
            throw new  SeguridadException( ex.toString( ) );
        }
    }

    public String getFile()
    {
        return path;
    }

    public void setFile( final String path )
    {
        this.path = path;
    }

    public String byteToHex( final String data ) throws SeguridadException
    {
        if( data == null )
        {
            throw new SeguridadException( "Data is null" );
        }
        final String res = byteToHex( stringToByte( data ) );
        return res;
    }

    private String byteToHex( final byte[] data ) throws SeguridadException
    {
        if( data == null )
        {
            throw new SeguridadException( "Data is null" );
        }
        final StringBuffer stringBuffer = new StringBuffer( );
        for (int i = 0; i < data.length; i++)
        {
            int	v = data[ i ] & 0xff;
            stringBuffer.append( digits.charAt( v >> 4 ) );
            stringBuffer.append( digits.charAt( v & 0xf ) );
        }
        return stringBuffer.toString( );
    }

    public byte[ ] hexToByte( final byte bytes[ ] ) throws SeguridadException
    {
        if( bytes == null )
        {
            throw new SeguridadException( "Data is null" );
        }
        byte resBytes[] = new byte[ bytes.length / 2 ];
        Integer aux = null;
        try
        {
            for (int i = 0; i < ( bytes.length / 2 ); i++)
            {
                if( i >126 )
                {
                    //System.out.println(  "0x" + (char)bytes[ i * 2 ] + (char)bytes[ ( i * 2 ) + 1 ] );
                    //System.out.println( (int)bytes[ i * 2 ] );
                    //System.out.println( (int)bytes[ ( i * 2 ) + 1 ]  );
                }
                aux = Integer.decode( "0x" + (char)bytes[ i * 2 ] + (char)bytes[ ( i * 2 ) + 1 ] );
                int value = aux.intValue ();
                if (value > 127)
                {
                    value = value - 256;
                }
                resBytes[ i ] = new Integer( value ).byteValue( );

            }
        }
        catch( Exception ex )
        {
            throw new SeguridadException(ex.toString( ) );
        }
        return resBytes;
    }

    public String hexToByte( final String data )  throws SeguridadException
    {
        if( data == null )
        {
            throw new SeguridadException( "Data is null" );
        }
        final byte[] bytes = hexToByte( stringToByte( data ) );
        return byteToString( bytes );
    }
}